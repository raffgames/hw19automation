import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.Alert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;
import java.util.UUID;



public class Main {
    WebDriver driver;



    @BeforeClass
    public static void setup(){
        System.setProperty("webdriver.gecko.driver","D:\\Hillel\\driver\\geckodriver.exe");
    }
    @Before
    public void precondition(){
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    static String generateString(int n)
    {


        String NumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            int index
                    = (int)(NumericString.length()
                    * Math.random());

            sb.append(NumericString
                    .charAt(index));
        }

        return sb.toString();
    }
    static String generateNames(int n)
    {


        String NumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"+ "abcdefghijklmnopqrstuvxyz";

        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            int index
                    = (int)(NumericString.length()
                    * Math.random());

            sb.append(NumericString
                    .charAt(index));
        }

        return sb.toString();
    }
    static String generateNumbers(int n)
    {


        String NumericString =  "0123456789";

        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            int index
                    = (int)(NumericString.length()
                    * Math.random());

            sb.append(NumericString
                    .charAt(index));
        }

        return sb.toString();
    }
    static String generatePosition(int n)
    {


        String NumericString = "mqd";

        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            int index
                    = (int)(NumericString.length()
                    * Math.random());

            sb.append(NumericString
                    .charAt(index));
        }

        return sb.toString();
    }


    protected void safeAlertAccept() {
        try {
            driver.switchTo().alert().accept();
        } catch (NoAlertPresentException e) {
        }
    }
    @Test
    public void openGooglePage() throws InterruptedException {
        // ЗАДАНИЕ В КЛАССЕ
        /*  driver.get("http://todomvc.com/examples/backbone/");
        int task = 1;
        for (int i = 0; i < 5; i++) {

            driver.findElement(By.cssSelector(".new-todo")).sendKeys("task"+task, Keys.ENTER);
            task+=1;
        }
        */
        // ДОМАШНЕЕ ЗАДАНИЕ

        driver.get("https://user-data.hillel.it/html/registration.html");
        String firstName = generateNames(7);
        String mail = generateString(5)+"@gmail.com";
        String password = generateString(9) +generateNumbers(1);
        driver.findElement(By.cssSelector(".registration")).click();
        driver.findElement(By.cssSelector("#first_name")).sendKeys(firstName, Keys.ENTER);
        driver.findElement(By.cssSelector("#last_name")).sendKeys(generateNames(7),Keys.ENTER);
        driver.findElement(By.cssSelector("#field_work_phone")).sendKeys(generateNumbers(5),Keys.ENTER);
        driver.findElement(By.cssSelector("#field_phone")).sendKeys("38066"+generateNumbers(7),Keys.ENTER);
        driver.findElement(By.cssSelector("#field_email")).sendKeys(mail,Keys.ENTER);
        driver.findElement(By.cssSelector("#field_password")).sendKeys(password,Keys.ENTER);
        driver.findElement(By.id(Integer.parseInt(generateNumbers(1)) <= 4 ? "male" : "female")).click();
        driver.findElement(By.id("position")).sendKeys(generatePosition(1),Keys.ENTER);
        driver.findElement(By.id("button_account")).click();
        System.out.println(mail);
        System.out.println(password);
        System.out.println(firstName);
        Thread.sleep(1000);
        safeAlertAccept();
        driver.navigate().to("https://user-data.hillel.it/html/registration.html");
        driver.findElement(By.cssSelector("#email")).sendKeys(mail, Keys.ENTER);
        driver.findElement(By.cssSelector("#password")).sendKeys(password, Keys.ENTER);
        driver.findElement(By.cssSelector(".login_button")).click();
        Thread.sleep(1000);
        driver.findElement(By.cssSelector("#employees")).click();
        driver.findElement(By.cssSelector("#first_name")).sendKeys(firstName, Keys.ENTER);
        driver.findElement(By.cssSelector("#search")).click();
        Thread.sleep(3000);
    }
    @After
    public void postcondition(){
        driver.quit();
    }

}
